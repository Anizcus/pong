﻿using System;
using System.Timers;

namespace Pong
{
    public class Ball : Entity
    {
        private int directionX;
        private int directionY;

        private double interval_delta = 0.1;
        private double interval;

        private Timer timer;

        public Ball(int x, int y, double interval, char symbol) : base(x, y, symbol)
        {
            directionX = -1;
            directionY = -1;

            this.interval = interval;

            timer = new Timer();
            timer.Elapsed += Move;
            updateInterval();
            timer.Start();
        }

        private void updateInterval()
        {
            timer.Interval = interval * 1000;
        }

        public void increaseSpeed()
        {
            if(interval - interval_delta <= 0)
            {
                interval = interval_delta;
            } else
            {
                interval -= interval_delta;
            }
        }

        private void Move(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            x += directionX;
            y += directionY;
            Manager.getInstance().requestRepaint();

            updateInterval();
        }

        public int deltaY()
        {
            return y + directionY;
        }

        public int deltaX()
        {
            return x + directionX;
        }

        public void reverseX()
        {
            directionX *= -1;
        }

        public void reverseY()
        {
            directionY *= -1;
        }

        public override void Render()
        {
            Console.SetCursorPosition(x, y);
            Console.Write(symbol);
        }
    }
}
