﻿namespace Pong
{
    public class BallRules
    {
        private Ball ball;

        public BallRules(Ball ball)
        {
            this.ball = ball;
        }

        public bool BallhitsPaddle(Ball ball, Paddle paddle)
        {
            return (paddle.X == ball.deltaX()) && (ball.deltaY() >= paddle.Y && ball.deltaY() <= paddle.Y + paddle.getSize());
        }

        public bool BallhitsBorderWithY(Ball ball, int min, int max)
        {
            return ball.deltaY() >= max || ball.deltaY() <= min;
        }

        public bool BallhitsBorderWithY(int min, int max)
        {
            return BallhitsBorderWithY(ball, min, max);
        }

        public bool BallhitsBorderWithX(Ball ball, int min, int max)
        {
            return ball.deltaX() >= max || ball.deltaX() <= min;
        }

        public bool BallhitsBorderWithX(int min, int max)
        {
            return BallhitsBorderWithX(ball, min, max);
        }

        public bool isPaddleAbleToFollowBall(Paddle paddle, Ball ball, int min, int max)
        {
            return (ball.Y + paddle.getSize() < max && ball.Y > min);
        }

        public bool isPaddleAbleToFollowBall(Paddle paddle, int min, int max)
        {
            return isPaddleAbleToFollowBall(paddle, ball, min, max);
        }
    }
}
