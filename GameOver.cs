﻿using System;

namespace Pong
{
    public class GameOver : State
    {
        private string[] message;


        public GameOver(Manager manager, int score) : base(manager)
        {
            message = new string[] {
                "Game Over! Your Score: " + score,
                "Press any key to continue."
            };
        }

        public override void Input(ConsoleKey key)
        {
            manager.setState(new Menu(manager));
        }

        public override void Render()
        {

            for(int height = 0; height < message.Length; height++)
            {
                Console.SetCursorPosition(manager.getWidth() / 2, manager.getHeight() / 2 + height);
                Console.WriteLine(message[height]);
            }
        }

        public override void Update()
        {
            
        }
    }
}
