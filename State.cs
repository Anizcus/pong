﻿using System;

namespace Pong
{
    public abstract class State
    {
        protected Manager manager;

        public State(Manager manager)
        {
            this.manager = manager;
        }

        public abstract void Input(ConsoleKey key);
        public abstract void Update();
        public abstract void Render(); 
    }
}
