﻿using System;

namespace Pong
{
    public class Manager 
    {
        private static Manager instance;

        private State state;
        private bool repaint;

        private int width, height;

        private Manager(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }

        public static Manager getInstance()
        {
            if(instance == null)
            {
                instance = new Manager(50, 15);
                instance.requestRepaint();
            }
            return instance;
        }

        public void setState(State state)
        {
            this.state = state;
            repaint = true;
        }

        public State getState()
        {
            return state;
        }

        public void requestRepaint()
        {
            repaint = true;
        }

        public void processInput()
        {
            if(state != null)
            {
                while (Console.KeyAvailable)
                {
                    state.Input(Console.ReadKey(true).Key);
                }
            }
        }

        public void processUpdate()
        {
            if(state != null)
            {
                state.Update();
            }
        }

        public void processPaint()
        {
            if(state != null && repaint)
            {
                Console.Clear();
                state.Render();
                repaint = false;
            }
        }
    }
}
