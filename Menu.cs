﻿using System;

namespace Pong
{
    class Menu : State
    {
        private String[] options;
        private String message;
        private int selected, centerX, centerY, messageY;

        public Menu(Manager manager) : base(manager)
        {
            selected = 0;
            message = "Welcome to this Game!";
            options = new String[] { "Start Game", "Exit" };

            centerX = manager.getWidth() / 2;
            centerY = manager.getHeight() / 2;
            messageY = 3;
        }

        public override void Input(ConsoleKey key)
        {
            if(key == ConsoleKey.UpArrow)
            {
                selected--;
                if(selected < 0)
                {
                    selected = options.Length - 1;
                }
                manager.requestRepaint();
            } else if (key == ConsoleKey.DownArrow)
            {
                selected++;
                if (selected > options.Length - 1)
                {
                    selected = 0;
                }
                manager.requestRepaint();
            } else if (key == ConsoleKey.Enter)
            {
                switch (selected)
                {
                    case 0: {
                            manager.setState(new Game(manager));
                        } break;
                    case 1:
                        {
                            Environment.Exit(0);
                        } break;
                }
            }
        }

        public override void Update()
        {

        }

        public override void Render()
        {
            Console.SetCursorPosition(centerX - message.Length / 2, messageY);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write(message);
            for(int height = 0; height < options.Length; height++)
            {
                Console.SetCursorPosition(centerX - options[height].Length / 2, centerY + height);
                if (height == selected)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                } else
                {
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                Console.Write(options[height]);
            }
        }

    }
}
