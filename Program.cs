using System;

namespace Pong
{
	class Program
	{
		static void Main(string[] args)
		{
            Manager manager = Manager.getInstance();
            manager.setState(new Menu(manager));

            Console.CursorVisible = false;

            while (true)
            {
                manager.processInput();
                manager.processUpdate();
                manager.processPaint();
            }
        }
	}
}
