﻿using System;
using System.Timers;

namespace Pong
{
    public class Paddle : Entity
    {
        private int size;

        public Paddle(int x, int y, int size, char symbol) : base(x, y, symbol)
        {
            this.size = size;
        }

        public int getSize()
        {
            return size - 1;
        }

        public override void Render()
        {
            for(int height = 0; height < size; height++)
            {
                Console.SetCursorPosition(x, y + height);
                Console.Write(symbol);
            }
        }
    }
}
