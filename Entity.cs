﻿namespace Pong
{
    public abstract class Entity
    {
        protected int x;
        protected int y;
        protected char symbol;

        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }
        public char Symbol { get => symbol; }

        public Entity(int x, int y, char symbol)
        {
            this.x = x;
            this.y = y;
            this.symbol = symbol;
        }

        public abstract void Render();
    }
}
