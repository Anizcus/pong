﻿using System;

namespace Pong
{
    public class Game : State
    {
        private Ball ball;
        private Paddle player;
        private Paddle computer;
        private BallRules rules;

        private int player_score;
        private int centerX, centerY;

        private string score_msg;

        private const char border_icon = '#';
        private const char middle_icon = ':';

        public Game(Manager manager) : base(manager)
        {
            centerY = manager.getHeight() / 2;
            centerX = manager.getWidth() / 2;

            player_score = 0;
            score_msg = "Score: ";

            player = new Paddle(3, centerY, 3, '|');
            ball = new Ball(centerX, centerY, 0.1, 'O');
            computer = new Paddle(manager.getWidth() - 3, centerY, 3, '|');

            rules = new BallRules(ball);
        }

        private void PaintBorder()
        {
            int width = manager.getWidth();
            int height = manager.getHeight();

            Console.SetCursorPosition(0, 0);
            for (int x = 0; x < width; x++)
            {
                Console.Write(border_icon);
            }

            for (int y = 0; y < height; y++)
            {
                Console.WriteLine(border_icon);
            }

            Console.SetCursorPosition(0, height);
            for (int x = 0; x < width; x++)
            {
                Console.Write(border_icon);
            }

            for (int y = 1; y <= height; y++)
            {
                Console.SetCursorPosition(width, y);
                Console.Write(border_icon);
            }

            for (int y = 1; y < height; y++)
            {
                Console.SetCursorPosition(width / 2, y);
                Console.Write(middle_icon);
            }
        }

        public override void Input(ConsoleKey key)
        {
            if(key == ConsoleKey.Escape)
            {
                manager.setState(new Menu(manager));
            }else if(key == ConsoleKey.DownArrow)
            {
                if(player.Y + player.getSize() + 1 < manager.getHeight())
                    player.Y++;
                manager.requestRepaint();
            } else if (key == ConsoleKey.UpArrow)
            {
                if (player.Y - 1 > 0)
                    player.Y--;
                manager.requestRepaint();
            }
        }

        public override void Render()
        {
            PaintBorder();
            ball.Render();
            player.Render();
            computer.Render();

            Console.SetCursorPosition(centerX, manager.getHeight() + 1);
            Console.Write(score_msg + player_score);
        }

        public override void Update()
        {
            if (rules.isPaddleAbleToFollowBall(computer, 0, manager.getHeight()))
            {
                computer.Y = ball.Y;
            }

            if (rules.BallhitsPaddle(ball, player))
            {
                ball.reverseX();
                ball.increaseSpeed();
                player_score++;
            }else if (rules.BallhitsPaddle(ball, computer))
            {
                ball.reverseX();
            }

            if (rules.BallhitsBorderWithX(0, manager.getWidth()))
            {
                ball.reverseX();
                if(ball.X < centerX)
                {
                    manager.setState(new GameOver(manager, player_score));
                }
            }

            if (rules.BallhitsBorderWithY(0, manager.getHeight()))
            {
                ball.reverseY();
            }
        }
    }
}
